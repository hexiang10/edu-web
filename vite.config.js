import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import ElementPlus from 'unplugin-element-plus/vite'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import path from 'path'

const pathSrc = path.resolve(__dirname, 'src')

// https://vitejs.dev/config/
export default defineConfig({
  // 服务器配置
  server: {
    // 服务器主机名，默认是 localhost
    host: '0.0.0.0',
    // 端口号，默认是 5173
    port: 8080,
    //为开发服务器配置 CORS , 默认启用并允许任何源
    cors: true,
    //是否强制依赖预构建
    force: true,
    // 是否开启 https
    https: false,
    // 服务器代理配置
    // proxy: {
    //     // 如果请求的路径符合该正则表达式，则会被代理到 target 中
    //     // 例如请求 /api/user 会被代理到 http://localhost:8888/api/user
    //     '^/api': {
    //         target: 'http://localhost:8888',
    //         changeOrigin: true,
    //         rewrite: (path) => path.replace(/^\/api/, ''),
    //     },
    // },
    // 自定义中间件
    middleware: [],
    // 是否开启自动刷新
    // hmr: true,
    // 是否开启自动打开浏览器
    open: true
  },
  resolve: {
    alias: {
      '@': pathSrc
    },
    // 导入时想要省略的扩展名列表
    // 不建议使用 .vue 影响IDE和类型支持
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json']
  },
  plugins: [
    vue(),
    AutoImport({
      resolvers: [
        ElementPlusResolver(),
        // 自动导入图标组件
        IconsResolver({
          prefix: 'Icon'
        })
      ],
      dts: path.resolve(pathSrc, 'auto-imports.d.ts'),
      imports: ['vue', 'vue-router', { axios: [['default', 'axios']] }]
    }),
    Components({
      resolvers: [
        ElementPlusResolver(),
        // 自动注册图标组件
        IconsResolver({
          enabledCollections: ['ep']
        })
      ],
      dts: path.resolve(pathSrc, 'components.d.ts')
    }),
    ElementPlus({
      // options
    }),
    Icons({
      autoInstall: true
    })
  ],
  //scss全局变量一个配置
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "@/styles/mixin.scss";'
      }
    }
  },
  // 强制预构建插件包
  optimizeDeps: {
    //检测需要预构建的依赖项
    entries: [],
    //默认情况下，不在 node_modules 中的，链接的包不会预构建
    include: ['axios']
  },
  build: {}
})
