class IndexTestData {
  constructor(res) {
    res.topLinks = this.getTopLinks().data
    res.carouselImages = this.getCarouselImages().data
  }
  /**
   * 获取顶部链接信息
   *
   * @returns 返回包含链接信息的对象，包含状态码和链接数据数组
   */
  getTopLinks() {
    return {
      data: [
        {
          url: 'https://www.zcst.edu.cn/',
          content: '学校官网'
        },
        {
          url: 'https://library.jluzh.edu.cn/',
          content: '图书馆'
        },
        {
          url: 'https://vpn.zcst.edu.cn/',
          content: '校内资源访问'
        },
        {
          url: 'http://127.0.0.1:8080/test',
          content: '测试页面'
        }
      ]
    }
  }

  /**
   * 获取轮播图图片列表
   *
   * @returns 包含状态码和图片URL列表的对象
   */
  getCarouselImages() {
    return {
      data: [
        'https://s21.ax1x.com/2024/03/29/pFo0cAH.jpg',
        'https://s21.ax1x.com/2024/03/29/pFo024A.png',
        'https://s21.ax1x.com/2024/03/29/pFo04jf.jpg',
        'https://s21.ax1x.com/2024/03/29/pFo0hgP.jpg',
        'https://s21.ax1x.com/2024/03/29/pFo0Iu8.jpg'
      ]
    }
  }
}

export default IndexTestData