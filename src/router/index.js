import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/index',
    component: () => import(/* webpackChunkName: "index" */ '@/views/index.vue')
  },
  {
    path: '/test',
    component: () => import('@/test/test.vue')
  },
  {
    path: '/error/401',
    component: () => import('@/views/error-page/401.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    component: () => import('@/views/error-page/404.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
