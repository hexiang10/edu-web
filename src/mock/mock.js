import IndexTestData from '@/test/script/indexTestData'

class MockApi {
  indexData(res) {
    new IndexTestData(res)
    return res
  }
}

export default MockApi
