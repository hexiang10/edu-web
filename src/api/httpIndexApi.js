import http from '@/service'
import mock from '@/mock'

// 使用 Promise 来确保异步操作完成
const httpIndexApi = async (result) => {
  // 如果是开发环境，使用 mock 数据
  if (import.meta.env.VITE_APP_MOCK == 'yes') {
    console.log('Using mock data for index page')
    return mock.indexData(result)
  }
  // 如果是生产环境，使用真实数据
  try {
    result.topLinks = await getTopLinks()
    result.carouselImages = await getCarouselImages()
  } catch (error) {
    console.error('Error initializing HTTP index:', error)
    throw error // 重新抛出错误，以便调用者可以处理它
  }
  return result
}
// 获取顶部链接
const getTopLinks = async () => {
  try {
    const res = await http.get({
      url: '/index/getTopLinks'
    })
    return res
  } catch (error) {
    console.error('Error fetching top links:', error)
    throw error // 重新抛出错误，以便调用者可以处理它
  }
}
// 获取轮播图
const getCarouselImages = async () => {
  try {
    const res = await http.get({
      url: '/index/getCarouselImages'
    })
    return res
  } catch (error) {
    console.error('Error fetching carousel images:', error)
    throw error // 重新抛出错误，以便调用者可以处理它
  }
}

export default httpIndexApi
