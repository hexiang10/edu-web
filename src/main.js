import { createApp } from 'vue'
import { createPinia } from 'pinia'
import router from './router'
import App from './App.vue'
import 'normalize.css/normalize.css' // a modern alternative to CSS resets
import '@/styles/index.scss'
import './style.css'


const app = createApp(App)
app.use(router).use(createPinia())
app.mount('#app')
