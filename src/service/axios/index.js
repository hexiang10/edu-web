import axios from 'axios'
class AxiosApi {
  constructor(config) {
    // 创建axios实例
    this.instance = axios.create(config)
    // 保存基本信息
    this.interceptors = config.interceptors

    // 使用拦截器
    // 1.从config中取出的拦截器是对应的实例的拦截器
    this.instance.interceptors.request.use(
      this.interceptors?.requestInterceptor,
      this.interceptors?.requestInterceptorCatch
    )
    this.instance.interceptors.response.use(
      this.interceptors?.responseInterceptor,
      this.interceptors?.responseInterceptorCatch
    )
    // 请求拦截器
    this.instance.interceptors.request.use(
      (config) => {
        // console.log('======所有实例请求成功的拦截======')
        return config
      },
      (error) => {
        // 对请求错误做些什么
        // console.log('======所有实例请求失败的拦截======')
        return error
      }
    )
    // 响应拦截器
    this.instance.interceptors.response.use(
      (response) => {
        // console.log('======所有实例响应成功的拦截======')
        // 拦截响应的数据
        if (response.data.code === 200) {
          return response.data.data
        }
        return response.data
      },
      (error) => {
        // console.log('======所有实例响应失败的拦截======')
        return error
      }
    )
  }

  request(config) {
    return new Promise((resolve, reject) => {
      // 1.单个请求对请求config的处理
      if (config.interceptors?.requestInterceptor) {
        config = config.interceptors.requestInterceptor(config)
      }
      //Todo 2.判断是否需要显示loading

      this.instance
        .request(config)
        .then((res) => {
          // 1.单个请求对数据的处理
          if (config.interceptors?.responseInterceptor) {
            res = config.interceptors.responseInterceptor(res)
          }
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }
  get(config) {
    return this.request({ ...config, method: 'GET' })
  }
  post(config) {
    return this.request({ ...config, method: 'POST' })
  }
  put(config) {
    return this.request({ ...config, method: 'PUT' })
  }
  delete(config) {
    return this.request({ ...config, method: 'DELETE' })
  }
  patch(config) {
    return this.request({ ...config, method: 'PATCH' })
  }
  head(config) {
    return this.request({ ...config, method: 'HEAD' })
  }
  options(config) {
    return this.request({ ...config, method: 'OPTIONS' })
  }
}

export default AxiosApi
