// service统一出口
import AxiosApi from './axios'
import router from '@/router'

const http = new AxiosApi({
  baseURL: import.meta.env.VITE_APP_BASEURL,
  timeout: 10000,
  interceptors: {
    requestInterceptor: (config) => {
      // 在发送请求之前做些什么
      // 例如，添加token到请求头
      const token = localStorage.getItem('token')
      if (token) {
        config.headers.Authorization = `Bearer ${token}`
      }
      // console.log('======请求成功的拦截======')
      return config
    },
    requestInterceptorCatch: (err) => {
      // console.log('======请求失败的拦截======')
      return err
    },
    responseInterceptor: (res) => {
      // console.log('======响应成功的拦截======')
      return res
    },
    responseInterceptorCatch: (err) => {
      // console.log('======响应失败的拦截======')
      const { response } = err
      if (response) {
        errorHandle(response.status, response.data)
      }
      return err
    }
  }
})
/**
 * 响应已接收，但是不在2xx的范围
 * @param {Number} code 响应接收的状态码
 */
const errorHandle = (code, message) => {
  // 状态码判断
  switch (code) {
    // 401: 未登录状态，跳转登录页
    case 401:
      // toLogin()
      break
    // 403 token过期
    // 清除token并跳转登录页
    case 403:
      console.log('登录过期，请重新登录')
      localStorage.removeItem('token')
      // store.commit('loginSuccess', null)
      setTimeout(() => {
        toLogin()
      }, 1000)
      break
    // 404请求不存在
    case 404:
      console.log('请求资源不存在')
      to404()
      break
    default:
      console.log(message)
  }
}

/**
 * 跳转登录页
 * 携带当前页面路由，以期在登录页面完成登录后返回当前页面
 */
const toLogin = () => {
  router.replace({
    path: '/login',
    query: {
      redirect: router.currentRoute.fullPath
    }
  })
}
/**
 * 跳转登录页
 * 携带当前页面路由，以期在登录页面完成登录后返回当前页面
 */
const to404 = () => {
  router.replace({
    path: '/error/404',
    query: {
      redirect: router.currentRoute.fullPath
    }
  })
}

export default http
